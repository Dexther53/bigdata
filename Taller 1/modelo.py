from sqlalchemy.sql.schema import ForeignKey
import db
from sqlalchemy import Column, Integer, Text, String, ForeignKey, Float

class Departamento(db.Base):
    __tablename__ = "departamento"
    id = Column(Integer, primary_key=True)
    departamento = Column("departamento", Text)
    
class Municipio(db.Base):
    __tablename__ = "municipio"
    codigodane = Column("codigodane", String(10), primary_key=True)
    municipio = Column("municipio", Text)
    id_dept = Column(Integer, ForeignKey("departamento.id"))

class Incautacion(db.Base):
    __tablename__ = "incautacion"
    id = Column(Integer, primary_key=True)
    clasebien = Column("clasebien", Text)
    fecha = Column("fecha", Text)
    cantidad = Column("cantidad", Float)
    id_muni = Column(String(10), ForeignKey("municipio.codigodane"))

# Crear Modelos

def add(modelo):
    db.session.add(modelo)
    db.session.commit()

def buscar_dept(departamento):
    dept = db.session.query(Departamento).where(Departamento.departamento==departamento).first()
    return dept.id

def buscar_muni(municipio):
    muni = db.session.query(Municipio).where(Municipio.municipio==municipio).first()
    return muni.codigodane

def modelo_ina(codigodane, clasebien, fecha, cantidad):
    inca = Incautacion(
        clasebien=clasebien,
        fecha=fecha,
        cantidad=cantidad,
        id_muni=codigodane
    )
    return inca

def modelo_dpt(departamento):
    dept = Departamento(
        departamento=departamento
    )
    return dept

def modelo_muni(codigodane, municipio, id_dept):
    muni = Municipio(
        codigodane=codigodane,
        municipio=municipio,
        id_dept=id_dept
    )
    return muni
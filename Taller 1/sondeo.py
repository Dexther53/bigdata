import sys
import db
from modelo import *

lstDpt = []
lstMuni = []


if __name__ == "__main__":

    db.Base.metadata.create_all(db.motor)
    arg = sys.argv
    if len(arg) == 3:
        doc = arg[1]
        reg = arg[2]
        data_length = int(reg)
        file = open(doc, mode="r")
        pBreak = 0
        ignorar = True
        for line in file:
            line = line.strip()
            date = line.split(",")
            if ignorar:
                ignorar = False
            else:
                departamento = date[0]
                municipio = date[1]
                codigodane = date[2]
                clasebien = date[3]
                fecha = date[4]
                cantidad = float(date[5].replace(".",""))

                if not departamento in lstDpt:
                    lstDpt.append(departamento)
                    dept_model = modelo_dpt(departamento)
                    add(dept_model)


                if not municipio in lstMuni:
                    lstMuni.append(municipio)
                    id_dept = buscar_dept(departamento)
                    if id_dept:
                        muni_model = modelo_muni(codigodane, municipio, id_dept)
                        add(muni_model)
                    else:
                        print(f"No se encuentra el departamento {departamento}")


                id_muni = buscar_muni(municipio)
                if id_muni:
                    inca_model = modelo_ina(id_muni, clasebien, fecha, cantidad)
                    add(inca_model)
                else:
                    print(f"No se encuentra el municipio {municipio}")

            if pBreak >= data_length:
                print("Se insertaron de forma exitosa")
                break
            pBreak += 1

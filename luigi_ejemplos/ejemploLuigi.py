import luigi
#python3 ejemploluigi.py Cuadrados --local-scheduler
class SalidaNumeros(luigi.Task):

    def requires(self):
        return[]
    def output(self):
         return luigi.LocalTarget("numeros.txt")
    def run(self):
        with self.output().open('w') as f:
            for i in range(1,11):
                f.write('{}\n'.format(i))
                
class Cuadrados(luigi.Task):
    def requires(self):
        return [SalidaNumeros()]
    def output(self):
         return luigi.LocalTarget("cuadrados.txt")
    def run(self):
        with self.input()[0].open() as fin, self.output().open('w') as fout:
            for linea in fin:
                n= int(linea.strip())
                val = n**2
                fout.write('{}:{}\n'.format(n, val))
                
if __name__=='__main__':
    luigi.run()
   